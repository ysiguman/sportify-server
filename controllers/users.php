<?php
/*
#################################
###
###		AUTHORS CONTROLLER
###
#################################
*/

use MVC as mvc;

class Users extends mvc\Controller
{

	public function __construct ()
	{
		include_once(DIR_MODEL."user.php");
		include_once(DIR_MODEL."sport_user.php");
	}

	public static function getAll ()
	{
		$users = User::getAll();
		
		echo json_encode($users);
	}

	public static function getUser ($id)
	{
		$user = User::getUser($id);
		
		echo json_encode($user);
	}


	public static function getUserSports ($idUser)
	{
		$user = User::getUser($id);
		
		echo json_encode($user)["sport"];
	}

	public static function delete ($idUser)
	{
		User::deleteUser($idUser);
	}

	public static function addSport ($id, $data)
	{
		$sport["user"] = $id;
		$sport = array_merge($sport, $data);

		print_r($sport);

		SportUser::create($sport);
	}

}

?>