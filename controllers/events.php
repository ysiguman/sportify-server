<?php
/*
#################################
###
###		AUTHORS CONTROLLER
###
#################################
*/

use MVC as mvc;

class Events extends mvc\Controller
{

	public function __construct ()
	{
		include_once(DIR_MODEL."event.php");
	}

	public static function getAll ()
	{
		$events = Event::getAll();
		
		echo json_encode($events);
	}

	public static function getEvent($id)
	{
		$event = Event::getEvent($id);
		
		echo json_encode($event);
	}

}

?>