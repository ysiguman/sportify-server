<?php
require 'mvc/autoload.php';

// ##############
//  > Variables globales
// ##############
define ('DIR_ROOT', __DIR__);
define ('DIR_CLASS', __DIR__.'/controllers/');
define ('DIR_MODEL', __DIR__.'/models/');
// define ('DIR_VIEW',  __DIR__.'/views/');


// ##############
//  > Inclusion des fichiers contenants les controlleurs et les modèles
// ##############
require DIR_MODEL . 'user.php';
require DIR_CLASS . 'users.php';

require DIR_MODEL . 'sport_user.php';

require DIR_MODEL . 'sport.php';


// ##############
//  > Declaration des Routes ==> URLs accessibles
// ##############

// Initialisation du router
$router = new Router();

$router->addRoute(BASESERV . "/", "Users#getAll", 'GET');
$router->addRoute(BASESERV . "/api/user/{i}", "Users#getUser", 'GET');
$router->addRoute(BASESERV . "/api/user/{i}/delete", "Users#delete", 'GET');
$router->addRoute(BASESERV . "/api/user/{i}/add", "Users#addSport", 'POST');
$router->addRoute(BASESERV . "/api/user/{i}/sports", "Users#getUserSports", 'GET');

// On lance la page appellée en donant l'url entrante en paramètre
$router->run($_SERVER['REQUEST_URI']);

?>