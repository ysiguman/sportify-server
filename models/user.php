<?php
/*
#################################
###
###		AUTHOR MODELE
###
#################################
*/

use MVC as mvc;

class User extends mvc\Model
{
	public $id;
	public $name;
	public $mail;
	public $sports;

	public function __construct($id = "", $name = "", $mail = 0, $sports)
	{
		$this->id = $id;
		$this->name = $name;
		$this->mail = $mail;
		$this->sports = $sports;
	}

	public static function getAll ()
	{
		$bdd = self::setBdd ();

		$sql = "SELECT * FROM user";

		$requete = $bdd->query($sql)->fetchAll();
		$users = [];

		foreach ($requete as $user) {
			$users[] = new User(
				$user["id"],
				$user["name"],
				$user["mail"],
				SportUser::getForUser($user["id"]));
		}

		return $users;
	}

	public static function getUser ($id)
	{
		$bdd = self::setBdd ();

		$sql = "SELECT * FROM user WHERE id = $id";

		$requete = $bdd->query($sql)->fetch();

		$user = new User(
				$requete["id"],
				$requete["name"],
				$requete["mail"],
				SportUser::getForUser($requete["id"])
			);

		return $user;
	}

	public static function deleteUser ($id)
	{
		$bdd = self::setBdd ();

		$sql = "DELETE FROM user WHERE id = $id";

		$bdd->exec($sql);
	}

}

?>