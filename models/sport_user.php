<?php
/*
#################################
###
###		AUTHOR MODELE
###
#################################
*/

use MVC as mvc;

class SportUser extends mvc\Model
{
	public $id;
	public $user;
	public $sport;
	public $level;
	public $comment;

	public function __construct($id = "", $sport = "", $level = "", $comment = "")
	{
		$this->id = $id;
		$this->sport = $sport;
		$this->level = $level;
		$this->comment = $comment;
	}

	public static function getForUser ($id)
	{
		$bdd = self::setBdd ();

		$sql = "SELECT * FROM sport_user WHERE fk_user_id = $id";

		$requete = $bdd->query($sql)->fetchAll();
		$sports = [];

		foreach ($requete as $sport) {
			$sports[] = new SportUser(
				$sport["id"],
				self::getSport($sport["fk_sport_id"]),
				$sport["level"],
				$sport["comment"]);
		}

		return $sports;
	}

	public static function getSport ($id) {
		return Sport::getSport($id);
	}


	public static function create ($data) {
		$bdd = self::setBdd ();
		try {
        	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        	$sql = "INSERT INTO sport_user (fk_user_id, fk_sport_id, level, comment)
    	    VALUES (".$data['user'].", ".
            	    $data['sportId'].", ".
            	    $data['level'].", '".
            	    $data['comment']."')";

           	//	On lance la requete
            $bdd->exec($sql);

		}
    	//	Si erreur il y a eu, l'affiche
    	catch(PDOException $e)
    	{
	    	echo $sql . "<br>" . $e->getMessage();
    	}

	}
}

?>