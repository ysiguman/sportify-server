<?php
/*
#################################
###
###		AUTHOR MODELE
###
#################################
*/

use MVC as mvc;

class Sport extends mvc\Model
{
	public $id;
	public $name;

	public function __construct($id = "", $name = "")
	{
		$this->id = $id;
		$this->name = $name;
	}

	public static function getAll ()
	{
		$bdd = self::setBdd ();

		$sql = "SELECT * FROM sport";

		$requete = $bdd->query($sql)->fetchAll();
		$sports = [];

		foreach ($requete as $sport) {
			$sports[] = new Sport(
				$sport["id"],
				$sport["name"]);
		}

		return $sports;
	}


	public static function getSport ($id)
	{
		$bdd = self::setBdd ();

		$sql = "SELECT * FROM sport WHERE id = $id";

		$requete = $bdd->query($sql)->fetch();

		$sport = new Sport(
			$requete["id"],
			$requete["name"]
		);

		return $sport;
	}

}

?>