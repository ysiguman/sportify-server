# README Sportify



# URLs dispo

 - `/` : [GET] tous les utilisateurs
 - `/api/user/{i}` : [GET] utilisateur via son id ( {i} )
 - `/api/user/{i}/sports` : [GET] sports d'un utilisateur
 - `/api/user/{i}/sports/add` : [POST] ajouter un sport à un utilisateur: json sous la forme: "{"sportId": 1, "level": 1, "comment": "test"}"
 - `/api/user/{i}/delete` : [GET] suppression d'un utilisateur

