<?php
	require "./mvc/mvc.php";
	require "./mvc/router.php";	

//	On charge la configuration
	$config_file = file_get_contents("./config.json"); // inclusiuon du fichier de configs
	$config = json_decode($config_file);

//	On définit les variables de configurations en variables globales
//	On choisit la convontion de les ecrire en majuscule
	define ('HOST', $config[0]->dbHost);
	define ('DB_USER', $config[0]->dbUser);
	define ('DB_PASS', $config[0]->dbPass);
	define ('DB_NAME', $config[0]->dbName);

	define('BASESERV', $config[0]->baseDir); //dossier de l'application
?>